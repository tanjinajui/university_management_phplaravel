<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/blogadmin','Admin\AdminController@dashboard');
//Department Controller
 Route::resource('/departments','Admin\DepartmentController');
 //Teacher Controller
 Route::resource('/teachers','Admin\TeacherController');
//Course Controller
 Route::resource('/courses','Admin\CourseController');
 //Semester Controller
 Route::resource('/semesters','Admin\SemesterController');
 //Divisions Controller
 Route::resource('/divisions','Admin\DivisionController');
 //district Controller
 Route::resource('/districts','Admin\DistrictController');
 //district Controller
 Route::resource('/upozilas','Admin\UpozilaController');
 //student Controller
 Route::resource('/students','Admin\StudentController');
 //course_asign Controller
 Route::resource('/courses_asigns','Admin\CourseAsignController');
 //course_asign Controller
 Route::resource('/enrollcourses','Admin\courseEnrollController');
 //course_asign Controller
 Route::resource('/courses_asigns_teacher','Admin\CourseAsignTeacherController');
 //jsondistrict Controller
 Route::get('/jsonDistricts','Admin\AddressController@viewDistrict');
 Route::get('/jsonUpozilas','Admin\AddressController@viewUpozila');
 Route::get('/jsonCourses','Admin\AddressController@viewCourse');
 Route::get('/jsonTeachers','Admin\AddressController@viewTeacher');
 Route::get('/jsonCourseCodes','Admin\AddressController@viewCourseCode');
 Route::get('/jsonCourseTeachers','Admin\AddressController@viewCourseTeacher');
 Route::get('/jsonSemesters','Admin\AddressController@viewSemester');
 Route::get('/jsonMaxteacherCredit','Admin\AddressController@maxTeacherCredit');
 Route::get('/jsonCourseEnroll','Admin\AddressController@courseEnroll');
 Route::get('/jsonSelectCourseEnroll','Admin\AddressController@stdCourses');
 Route::get('/json-select_std_teacher','AddressController@stdTeacher');
	

