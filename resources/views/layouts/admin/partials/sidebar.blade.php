<!DOCTYPE html>
<html lang="en-us">
  <head>
   

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> @yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">


    <!-- lib-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic">
    <!--
    link(rel='stylesheet' href='assets/stylesheets/ionicons.css')
    link(rel='stylesheet' href='assets/stylesheets/font-awesome.css')
    link(rel='stylesheet' href='assets/stylesheets/weather-icons.min.css')
    link(rel='stylesheet' href='assets/stylesheets/animate.css')
    link(rel='stylesheet' href='assets/stylesheets/glyphicon.css')
    
    -->

    <!--
    plugin
    link(rel='stylesheet' href='assets/stylesheets/plugin/bootstrap-table.css')
    link(rel='stylesheet' href='assets/stylesheets/plugin/nifty-modal.css')
    link(rel='stylesheet' href='assets/stylesheets/plugin/jquery.bootstrap-touchspin.css')
    link(rel='stylesheet' href='assets/stylesheets/plugin/select2.css')
    link(rel='stylesheet' href='assets/stylesheets/plugin/multi-select.css')
    link(rel='stylesheet' href='assets/stylesheets/plugin/ladda.min.css')
    link(rel='stylesheet' href='assets/stylesheets/plugin/daterangepicker.css')
    link(rel='stylesheet' href='assets/stylesheets/plugin/jquery.timepicker.css')
    link(rel="stylesheet" href="assets/stylesheets/plugin/jqvmap.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/bootstrap-submenu.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/code.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/jquery.dataTables.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/dataTables.bootstrap.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/jquery.gridster.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/summernote.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/bootstrap-markdown.min.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/bootstrap-select.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/asColorPicker.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/bootstrap-datepicker.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/jquery-labelauty.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/owl.carousel.min.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/owl.theme.default.min.css")
    
    -->

    <!-- Theme-->
    <!-- Concat all lib & plugins css-->

    <link id="mainstyle" rel="stylesheet" href="{{asset('admin/assets/stylesheets/theme-libs-plugins.css')}}">
    <link id="mainstyle" rel="stylesheet" href="{{asset('admin/assets/stylesheets/skin.css')}}">

    <!-- Demo only-->
    <link id="mainstyle" rel="stylesheet" href="{{asset('admin/assets/stylesheets/demo.css')}}">

    <!-- This page only-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries--><!--[if lt IE 9]>
    <script src="assets/scripts/lib/html5shiv.js"></script>
    <script src="assets/scripts/lib/respond.js"></script><![endif]-->
    <script src="{{asset('admin/assets/scripts/lib/modernizr-custom.js')}}"></script>
    <script src="{{asset('admin/assets/scripts/lib/respond.js')}}"></script>
    <!-- Possible Classes
    ** Gradient style:
    * orchid
    * cadetblue
    * joomla
    * influenza
    * moss
    * mirage
    * stellar
    * servquick
    
    ** Flat style:
    * f-dark
    * f-dark-blue
    * f-blue
    * f-green
    
    ** Layout control
    * minibar
    * layout-drawer (changed the var on top)
    
    e.g
    <body class="moss layout-drawer">
    -->
  </head>

  <body class="orchid  ">

    <!-- #SIDEMENU-->
    <div class="mainmenu-block">
      <!-- SITE MAINMENU-->
      <nav class="menu-block">
        <ul class="nav">
          <li class="nav-item mainmenu-user-profile"><a href="user-profile.html">
              <div class="circle-box"><img src="{{asset('images/logo.png')}}" alt="">
                <div class="dot dot-success"></div>
              </div>
              <div class="menu-block-label"><b>Odalys Broussard</b><br>Managing Director</div></a></li>
        </ul>
        <div class="p-a-2"><span class="small pull-xs-right">This Month Earnings</span>
          <canvas id="demo-bar-dark-chart"></canvas>
        </div>
        <ul class="nav">
          <li class="nav-item"><a class="nav-link" href="index.html"><i class="icon ion-home"></i><div class="menu-block-label">Home</div></a></li>
          <li class="nav-item"><a class="nav-link" href="http://thusbox.com/doc/adminhero/" target="_blank"><i class="icon ion-ios-list-outline"></i>
              <div class="menu-block-label">Documentation</div></a></li>
          <li class="nav-item"><a class="nav-link" href="ui-widget.html"><i class="icon ion-pizza"></i>
              <div class="menu-block-label">Widgets</div></a></li>
          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-monitor"></i>
              <div class="menu-block-label">Dashboard<span class="label label-success">new</span></div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="dashboard-1.html">Dashboard Basic</a></li>
              <li class="nav-item"><a class="nav-link" href="dashboard-2.html">Game Dashboard<span class="label label-success">new</span></a></li>
            </ul>
          </li>
          <!--li.header GENERAL-->
          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-ios-grid-view-outline"></i>
              <div class="menu-block-label">Layouts</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="layout-drawer.html">Drawer layout</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-drawer-color-header.html">Drawer with header</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-basic.html">Basic layout</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-left-sidebar.html">Left Sidebar</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-left-fixed-sidebar.html">Left Fixed Sidebar</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-right-sidebar.html">Right Sidebar</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-right-fixed-sidebar.html">Right Fixed Sidebar</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-menu-collapsed.html">Menu Collapsed</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-menu-expended.html">Menu Expended</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-header.html">Header Styles</a></li>
            </ul>
          </li>
          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-grid"></i>
              <div class="menu-block-label">Tables</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="table-ui.html">Table UI</a></li>
              <li class="nav-item"><a class="nav-link" href="table-basic.html">Table Basic</a></li>
              <li class="nav-item"><a class="nav-link" href="table-bootstrap.html">Table Bootstrap</a></li>
              <li class="nav-item"><a class="nav-link" href="table-datatables.html">Table Datatables</a></li>
            </ul>
          </li>
          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-toggle"></i>
              <div class="menu-block-label">Form</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="form-basic.html">Form basic</a></li>
              <li class="nav-item"><a class="nav-link" href="form-advanced.html">Form advanced</a></li>
              <li class="nav-item"><a class="nav-link" href="form-layout.html">Form layout</a></li>
              <li class="nav-item"><a class="nav-link" href="form-validation.html">Form validation</a></li>
            </ul>
          </li>
          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-cube"></i>
              <div class="menu-block-label">UI Elements</div><span class="label label-success">new</span></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="ui-button.html">Buttons</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-chart.html">Chart</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-color.html">Color</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-dropdowns.html">Dropdowns</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-files.html">Files</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-labels-badges.html">Badges & Labels</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-messages.html">Messages</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-modal.html">Modal</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-navigation.html">Navigation</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-notifications.html">Notication</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-panel.html">Panel</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-progress.html">Progress</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-tab.html">Tabs</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-timeline.html">Timeline</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-tooltips-popovers.html">Tooltips & Popovers</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-editor.html">Editor</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-type.html">type</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-widget.html">Widget</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-scrollers.html">Scrollers</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-broadcard.html">Broadcard<span class="label label-success">new</span></a></li>
            </ul>
          </li>
          <!--li.header.nav-item APP-->
          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-document"></i>
              <div class="menu-block-label">Page view</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="page-tasks.html">Tasks</a></li>
              <li class="nav-item"><a class="nav-link" href="page-project-details.html">Project Details</a></li>
              <li class="nav-item"><a class="nav-link" href="page-media-lib.html">Media Library</a></li>
              <li class="nav-item"><a class="nav-link" href="page-tracker.html">Tracker</a></li>
              <li class="nav-item"><a class="nav-link" href="page-mail.html">Mail</a></li>
              <li class="nav-item"><a class="nav-link" href="page-inbox.html">Inbox</a></li>
              <li class="nav-item"><a class="nav-link" href="page-inbox-2.html">Inbox 2</a></li>
            </ul>
          </li>
          <li class="menu-block-has-sub nav-item "><a class="nav-link {{'departments'== request()->path() ? 'active' : ''}}" href="#"><i class="icon ion-android-funnel"></i>
          <div class="menu-block-label">Department</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item "><a class="nav-link {{'departments'== request()->path() ? 'active' : ''}} " href="/departments">All Departments</a></li>            
            </ul>
          </li> 
          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-android-funnel"></i>
          <div class="menu-block-label">Semester</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item {{'semesters'== request()->path() ? 'active' : ''}}"><a class="nav-link" href="/semesters">All Semesters</a></li>            
            </ul>
          </li>
          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-android-funnel"></i>
              <div class="menu-block-label">Teacher</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="/teachers/create">Teacher Add</a></li>            
              <li class="nav-item"><a class="nav-link" href="/teachers">All Teachers</a></li>            
            </ul>
          </li>
          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-android-funnel"></i>
            <div class="menu-block-label">Course</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="/courses/create">Course Add</a></li>            
              <li class="nav-item"><a class="nav-link" href="/courses">All Courses View</a></li>            
            </ul>
          </li> 
          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-android-funnel"></i>
            <div class="menu-block-label">Student</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="/students/create">Student Add</a></li>            
              <li class="nav-item"><a class="nav-link" href="/students">All Student View</a></li>            
            </ul>
          </li> 
          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-android-funnel"></i>
            <div class="menu-block-label">Course Asign</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="/courses_asigns/create">Course Asign</a></li>  
              <li class="nav-item"><a class="nav-link" href="/courses_asigns">All Course Asign View</a></li>   
            </ul>
          </li> 
          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-android-funnel"></i>
            <div class="menu-block-label">Course inroll</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="/enrollcourses/create">Course inroll</a></li>  
              <li class="nav-item"><a class="nav-link" href="/enrollcourses">All Course Asign View</a></li>   
            </ul>
          </li> 
          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-android-funnel"></i>
            <div class="menu-block-label">Course Asign Teacher</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="/courses_asigns_teacher/create">Course Asign Teacher</a></li>  
              <li class="nav-item"><a class="nav-link" href="/courses_asigns_teacher">All Course Asign Teacher View</a></li>   
            </ul>
          </li> 
          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-android-funnel"></i>
              <div class="menu-block-label">Address</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="/divisions">Division create/view</a></li>            
              <li class="nav-item"><a class="nav-link" href="/districts">District create/view</a></li>            
              <li class="nav-item"><a class="nav-link" href="/upozilas">Upozila create/view</a></li>            
            </ul>
          </li>
          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-android-people"></i>
              <div class="menu-block-label">User</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="user-login.html">Login</a></li>
              <li class="nav-item"><a class="nav-link" href="user-login-2.html">Login 2</a></li>
              <li class="nav-item"><a class="nav-link" href="user-login-3.html">Login 3</a></li>
              <li class="nav-item"><a class="nav-link" href="user-register.html">Register</a></li>
              <li class="nav-item"><a class="nav-link" href="user-register-2.html">Register 2</a></li>
              <li class="nav-item"><a class="nav-link" href="user-register-3.html">Register 3</a></li>
              <li class="nav-item"><a class="nav-link" href="user-register-4.html">Register 4</a></li>
              <li class="nav-item"><a class="nav-link" href="user-lists.html">Users Lists</a></li>
              <li class="nav-item"><a class="nav-link" href="user-profile.html">User Profile</a></li>
              <li class="nav-item"><a class="nav-link" href="user-profile-2.html">User Profile 2</a></li>
              <li class="nav-item"><a class="nav-link" href="user-profile-3.html">User Profile 3</a></li>
            </ul>
          </li>
          <!--li.header.nav-item user interface-->
          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-android-funnel"></i>
              <div class="menu-block-label">Menu Level</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="#">Commas</a></li>
              <li class="nav-item menu-block-has-sub"><a href="#">Neversa</a>
                <ul class="nav menu-block-sub">
                  <li class="nav-item"><a class="nav-link" href="#">Incurred</a></li>
                  <li class="nav-item"><a class="nav-link" href="#">Preparation</a></li>
                </ul>
              </li>
              <li class="nav-item"><a class="nav-link" href="#">Country</a></li>
            </ul>
          </li>
        </ul>
        <!-- END SITE MAINMENU-->
      </nav>
    </div>

     <!-- #MAIN-->
    <div class="main-wrapper">

      <!-- VIEW WRAPPER-->
      <div class="view-wrapper">

        <!-- TOP WRAPPER-->
        <div class="topbar-wrapper">

          <!-- NAV FOR MOBILE-->
          <div class="topbar-wrapper-mobile-nav"><a class="topbar-togger js-minibar-toggler" href="#"><i class="icon ion-ios-arrow-back hidden-md-down"></i><i class="icon ion-navicon-round hidden-lg-up"></i></a><a class="topbar-togger pull-xs-right hidden-lg-up js-nav-toggler" href="#"><i class="icon ion-android-person"></i></a>

            <!-- ADD YOUR LOGO HEREText logo: a.topbar-wrapper-logo(href="#") AdminHero
            --><a class="topbar-wrapper-logo demo-logo" href="/blogadmin">University</a>
          </div>
          <!-- END NAV FOR MOBILE-->

          <!-- SEARCH-->
          <div class="topbar-wrapper-search">
            <form>
              <input class="form-control" type="search" placeholder="Search"><a class="topbar-togger topbar-togger-search js-close-search" href="#"><i class="icon ion-close"></i></a>
            </form>
          </div>
          <!-- END SEARCH-->

          <!-- TOP RIGHT MENU-->
          <ul class="nav navbar-nav topbar-wrapper-nav">
            <li class="nav-item"><a class="nav-link js-search-togger" href="#"><i class="icon ion-ios-search-strong"></i></a></li>

            <li class="nav-item dropdown"><a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="icon fa fa-columns"></i><span class="badge badge-success status">4</span></a>

              <!-- #RIGHT BLOCK-->
              <!--
              RIGHT PANEL
              * same dropdown-menu markup, add '.dropdown-menu-side'
              -->
              <div class="dropdown-menu dropdown-menu-side">
                <ul class="nav nav-tabs nav-justified">
                  <li class="nav-item"><a class="nav-link active" data-target="#demoright-1" aria-controls="demoright-1" role="tab" data-toggle="tab"><i class="ion-help-buoy icon"></i></a></li>
                  <li class="nav-item"><a class="nav-link" data-target="#demoright-2" aria-controls="demoright-2" role="tab" data-toggle="tab"><i class="ion-quote icon"></i></a></li>
                  <li class="nav-item"><a class="nav-link" data-target="#demoright-3" aria-controls="demoright-3" role="tab" data-toggle="tab"><i class="ion-gear-b icon"></i></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active fade in" role="tabpanel" id="demoright-1"><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/2.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Ryan Sears</div>
                        <p class="text-muted small">Rome; Hard there to deeply verbal page goals a accept into it pri</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle">
                        <div class="media-object circle-object bg-info"><i class="ion-calendar"></i></div>
                      </div>
                      <div class="media-body">
                        <div class="media-heading">Event remind.</div>
                        <p class="text-muted small">10 min ago</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle">
                        <div class="media-object circle-object bg-success">40%</div>
                      </div>
                      <div class="media-body">
                        <div class="media-heading">Event remind.</div>
                        <div class="progress">
                          <progress class="progress progress-danger loader" value="46" max="100"></progress>
                        </div>
                        <p class="text-muted small">10 min ago</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle">
                        <div class="media-object circle-object bg-danger">2d</div>
                      </div>
                      <div class="media-body">
                        <div class="media-heading">Project deadline.</div>
                        <div class="progress">
                          <progress class="progress progress-success loader" value="80" max="100"></progress>
                        </div>
                        <p class="text-muted small">80% completion</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/1.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Judith Sullivan</div>
                        <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/2.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Judith Sullivan</div>
                        <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/3.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Judith Sullivan</div>
                        <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/4.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Judith Sullivan</div>
                        <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/1.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Judith Sullivan</div>
                        <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/2.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Judith Sullivan</div>
                        <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/3.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Judith Sullivan</div>
                        <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/4.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Judith Sullivan</div>
                        <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/1.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Judith Sullivan</div>
                        <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/2.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Judith Sullivan</div>
                        <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/3.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Judith Sullivan</div>
                        <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/4.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Judith Sullivan</div>
                        <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/1.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Judith Sullivan</div>
                        <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/2.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Judith Sullivan</div>
                        <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/3.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Judith Sullivan</div>
                        <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                      </div></a><a class="dropdown-item media circle-box" href="#">
                      <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/4.jpg" alt=""></div>
                      <div class="media-body">
                        <div class="media-heading">Judith Sullivan</div>
                        <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                      </div></a>
                  </div>
                  <div class="tab-pane p-a-1 fade" role="tabpanel" id="demoright-2"></div>
                  <div class="tab-pane p-a-1 fade" role="tabpanel" id="demoright-3"></div>
                </div>
              </div>
              <!-- END RIGHt PANEL-->
            </li>

            <li class="nav-item dropdown"><a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="icon ion-android-notifications-none"></i><span class="badge badge-danger status">9+</span></a>
              <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header">Notifications (5)</div>
                <div class="dropdown-menu-inner"><a class="dropdown-item media circle-box" href="#">
                    <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/2.jpg" alt=""></div>
                    <div class="media-body">
                      <div class="media-heading">Ryan Sears</div>
                      <p class="text-muted small">Pink do well together specially name if design postage briefs big into in her working</p>
                    </div></a><a class="dropdown-item media circle-box" href="#">
                    <div class="media-left media-middle">
                      <div class="media-object circle-object bg-danger"><i class="fa fa-bug"></i></div>
                    </div>
                    <div class="media-body">
                      <div class="media-heading">Server Error</div>
                      <p class="text-muted small">3 minutes ago</p>
                    </div></a><a class="dropdown-item media circle-box" href="#">
                    <div class="media-left media-middle">
                      <div class="media-object circle-object bg-success"><i class="fa fa-check"></i></div>
                    </div>
                    <div class="media-body">
                      <div class="media-heading">Server Error Reports</div>
                      <p class="text-muted small">3 minutes ago</p>
                    </div></a><a class="dropdown-item media circle-box" href="#">
                    <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/1.jpg" alt=""></div>
                    <div class="media-body">
                      <div class="media-heading">Judith Sullivan</div>
                      <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                    </div></a><a class="dropdown-item media circle-box" href="#">
                    <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/2.jpg" alt=""></div>
                    <div class="media-body">
                      <div class="media-heading">Judith Sullivan</div>
                      <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                    </div></a><a class="dropdown-item media circle-box" href="#">
                    <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/3.jpg" alt=""></div>
                    <div class="media-body">
                      <div class="media-heading">Judith Sullivan</div>
                      <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                    </div></a><a class="dropdown-item media circle-box" href="#">
                    <div class="media-left media-middle"><img class="media-object circle-object" src="assets/images/face/4.jpg" alt=""></div>
                    <div class="media-body">
                      <div class="media-heading">Judith Sullivan</div>
                      <p class="text-muted small">Option wouldn't small hardly of and more believe The fundamentals</p>
                    </div></a>
                </div><a class="dropdown-item" href="#">
                  <div class="text-xs-center"><i class="ion-more"></i></div></a>
              </div>
            </li>
            <li class="nav-item dropdown"><a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="icon ion-paintbucket"></i></a>
              <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                <div class="js-color-switcher demo-color-switcher">
                  <div class="dropdown-header">Flat</div>
                  <div class="list-inline"><a class="list-inline-item" href="#" data-color="f-dark">
                      <div class="demo-skin-grid f-dark"></div></a><a class="list-inline-item" href="#" data-color="f-dark-blue">
                      <div class="demo-skin-grid f-dark-blue"></div></a><a class="list-inline-item" href="#" data-color="f-blue">
                      <div class="demo-skin-grid f-blue"></div></a><a class="list-inline-item" href="#" data-color="f-green">
                      <div class="demo-skin-grid f-green"></div></a><a class="list-inline-item" href="#" data-color="f-deep-taupe">
                      <div class="demo-skin-grid f-deep-taupe"></div></a>
                  </div>
                  <div class="dropdown-header">Gradient</div>
                  <div class="list-inline"><a class="list-inline-item" href="#" data-color="orchid">
                      <div class="demo-skin-grid orchid"></div></a><a class="list-inline-item" href="#" data-color="cadetblue">
                      <div class="demo-skin-grid cadetblue"></div></a><a class="list-inline-item" href="#" data-color="joomla">
                      <div class="demo-skin-grid joomla"></div></a><a class="list-inline-item" href="#" data-color="influenza">
                      <div class="demo-skin-grid influenza"></div></a><a class="list-inline-item" href="#" data-color="moss">
                      <div class="demo-skin-grid moss"></div></a><a class="list-inline-item" href="#" data-color="mirage">
                      <div class="demo-skin-grid mirage"></div></a><a class="list-inline-item" href="#" data-color="stellar">
                      <div class="demo-skin-grid stellar"></div></a><a class="list-inline-item" href="#" data-color="servquick">
                      <div class="demo-skin-grid servquick"></div></a>
                  </div>
                </div>
              </div>
            </li>
            <li class="nav-item"><a class="nav-link" href="#"><i class="icon ion-android-exit"></i></a></li>
            <li class="nav-item"><a class="nav-link close-mobile-nav js-close-mobile-nav" href="#"><i class="icon ion-close"></i></a></li>
            <!-- END TOP RIGHT MENU-->
          </ul>
        </div>
        <!--END TOP WRAPPER-->
